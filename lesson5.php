<?php
//echo file_get_contents("test2");

//$file = "test";
//
//if(!file_exists($file) || !is_file($file))
//    die("File $file not found");
//
//echo file_get_contents($file); // Читает файл в строку
//
//$lines = file($file); // Возвращает строки в виде массива
//
//print_r(($lines));

$file = "greeting";
$str = "hello, world";

file_put_contents($file, $str, FILE_APPEND); //Запись строки в файл

$lines = file($file);
$count = count($lines);
$newText = "new";

for ($i = 0; $i < $count; $i++) {
    $lines[$i] = rtrim($lines[$i], "\t\n\r\0\x0B");

    $index = (int)($count / 2) - 1;
    if ($i == $index) {
        $lines[$i] .= "\n$newText";
    }
}

file_put_contents($file, implode("\n", $lines));

