<?php
error_reporting(E_ALL);
include_once "helpers.php";
$posts = include "data.php";
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php foreach ($posts as $title => $content)
    render("templates/post.php", [
        "title" => $title,
        "content" => $content,
    ]);

    render("template")
?>
</body>
</html>
