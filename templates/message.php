<div>
    <h1><?= $title ?? "No title" ?></h1>
    <p1><?= $message ?? "No message"?></p1>
    <?php if (isset($additional)): ?>
        <small><?= $additional ?></small>
    <?php endif; ?>
    <small><?= isset($additional) ? $additional : "" ?></small>
</div>