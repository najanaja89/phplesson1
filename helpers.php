<?php
function render($template, array $variables = [])
{
    $template = str_replace('/', '\\', $template);
    $template = str_replace('\\', DIRECTORY_SEPARATOR, $template);
    $template = ltrim($template, DIRECTORY_SEPARATOR);
    $template = rtrim($template, DIRECTORY_SEPARATOR);
    $template = getcwd() . DIRECTORY_SEPARATOR . $template;
    if (!file_exists($template) || !is_file($template))
        die("Template {$template} not found");

    if ($variables) {
        foreach ($variables as $variable => $value)
            $$variable = $value;
        include $template;
    }
}

function tag($name, array $attributes = [], $body = null, bool $selfClosing = false)
{
    $selfClosed = ["area", "base", "br", "embed", "hr", "iframe", "img",
        "input", "link", "meta", "param", "source", "track"];

    $selfClosing = in_array($name, $selfClosed) || $selfClosing;
    $tag = "<{$name}";

    $fAttr = function ($attr, $value = null) {
        $result = "";
        if (is_string($attr)) {
            $result = $attr;
        }
        if (is_string($attr) && $value) {
            $result .= "=";
        }
        if ($value) {
            $result .= "\"{$value}\"";
        }
    };
    foreach ($attributes as $attribute => $value) {
        $tag .= " " . $fAttr($attribute, $value);
    }

    return $tag . ($selfClosing ? " />" : ">{$body}</{$name}>");
}

function csvToArray(string $file, $delimiter = ','): array
{

    $res = [
        "count" => 0,
        "headers" => [],
        "data" => []
    ];

    if (!file_exists($file) || !is_file($file))
        return $res;

    $lines = file($file);
    if (0 == $count = count($lines))
        return $res;

    $res['count'] = $count - 1;
    $removeEol = function ($line) {
        $line = ltrim($line, "\t\n\r\0\x0B");
        return rtrim($line, "\t\n\r\0\x0B");
    };

    $res['headers'] = explode($delimiter, $removeEol(array_shift($lines)));

    foreach ($lines as $i => $line) {

        $res['data'][$i] = [];
        $line = explode($delimiter, $removeEol($line));

        foreach ($res['headers'] as $index => $header) {
            $res['data'][$i][$header] = $line[$index] ?? null;
        }
    }

    return $res;
}

function addToFile($filename, $data)
{
    if (!file_exists($filename) || !is_file($filename)) {
        die ("Wrong Data format or Filename");

    } else {
        if (is_string($data))
            file_put_contents($filename, $data, FILE_APPEND);
        if (is_array($data)) {
            foreach ($data as $key => $item) {
                file_put_contents($filename, "\n" . $item, FILE_APPEND);
            }
        }
    }
}

function getFromFile($filename)
{
     if (!file_exists($filename) || !is_file($filename)) {
         die ("Wrong Data format or Filename");

     } else {
         $file = file_get_contents($filename);
         return explode(" ",$file);
     }

}


